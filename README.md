# Age Verification Module

## Contents of This File

 - Introduction
 - Requirements
 - Installation
 - Configuration
 - Maintainers

## Introduction

The age gate verification module that forces the user to select a date
of birth before passing the user back to the requested URL.

 - For a full description of the module, visit the project page:
   <https://www.drupal.org/project/age_verification>

 - To submit bug reports and feature suggestions, or to track changes:
   <https://www.drupal.org/project/issues/age_verification>

## Requirements

This module requires nothing outside of drupal.

## Installation

 - Install as you would normally install a contributed Drupal module.
   Visit [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for further information.

## Configuration

 - `Admin->Config->System->Age Verification`.
 - You can select an different age by default is 21.
 - Relative URL paths can be used to bypass pages from the age verification.
   eg: you have a cookie policy page and don't want
   to act on, this could be the path of www.yoursite.com/cookie-policy
   simply add the relative URL `/cookie-policy`, `user` or `/node/[nid]`
 - The description field can be used to output text on the bottom of the form.
   eg: you have added a URL to ignore cookie page,
       you can also add HTML like below:
       `<p>This site uses cookies.</p>`
       `<a href="/cookie-policy" target="_blank">Cookie Policy</a>`
 - With both of these added you can have pages that are accessible without
   the age gate verification.

## Maintainers
 - Ed Walsh (BigEd) - <https://www.drupal.org/u/biged>
 - Tomasz Turczynski (Turek) - <https://www.drupal.org/u/turek>
 - Akshay Singh (akshay.singh) - <https://www.drupal.org/u/akshay-singh>
