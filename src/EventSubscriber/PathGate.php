<?php

namespace Drupal\age_verification\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Installer\InstallerKernel;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Http\RequestStack;

/**
 * Event Subscriber PathGate.
 */
class PathGate implements EventSubscriberInterface {
  /**
   * The path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The current patch.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * The request method.
   *
   * @var \Drupal\Core\Http\RequestStack
   */
  protected $request;

  /**
   * Config property.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The state property.
   *
   * @var Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new Redirect404Subscriber.
   *
   * @param \Drupal\Core\Path\PathMatcherInterface $pathMatcher
   *   The path matcher service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPath
   *   The current patch.
   * @param \Drupal\Core\Http\RequestStack $request
   *   The request property.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config property.
   * @param \Drupal\Core\State\StateInterface $state
   *   The stateInterface property.
   */
  public function __construct(PathMatcherInterface $pathMatcher, AccountInterface $current_user, CurrentPathStack $currentPath, RequestStack $request, ConfigFactoryInterface $configFactory, StateInterface $state) {
    $this->pathMatcher = $pathMatcher;
    $this->currentUser = $current_user;
    $this->currentPath = $currentPath;
    $this->request = $request;
    $this->configFactory = $configFactory;
    $this->state = $state;
  }

  /**
   * Code that should be triggered on event specified.
   */
  public function onRespond(FilterResponseEvent $event) {
    $session = $this->request->getCurrentRequest()->getSession();

    $age_verified = $session->get('age_verified');

    // If have a valid session.
    if ($age_verified == TRUE) {
      return;
    }
    // Make sure that it won't run when using cli(drush) or installing Drupal.
    if (PHP_SAPI === 'cli' || InstallerKernel::installationAttempted()) {
      return;
    }
    // Don't run when site is in maintenance mode.
    if ($this->state->get('system.maintenance_mode')) {
      return;
    }
    // Ignore non index.php requests(like cron).
    if (!empty($_SERVER['SCRIPT_FILENAME']) && realpath(DRUPAL_ROOT . '/index.php') != realpath($_SERVER['SCRIPT_FILENAME'])) {
      return;
    }
    // Get saved settings and other needed objects.
    $config = $this->configFactory->get('age_verification.settings');
    if ($config->get('age_verification_user_agents') !== NULL) {
      // Exploding the age_verification_user_agents field to separate lines.
      $user_agents = explode("\n", $config->get('age_verification_user_agents'));
      $http_user_agent = $this->request->getCurrentRequest()->server->get('HTTP_USER_AGENT');

      // Performing cleanup, trim white space and empty lines.
      foreach ($user_agents as $user_agent) {
        // To be sure we match proper string, we need to trim it.
        $user_agent = !empty($user_agent) ? trim($user_agent) : "";
        if ($http_user_agent == $user_agent) {
          return;
        }
      }
    }

    // Send to proper page if logged in.
    $skip_urls_config = $config->get('age_verification_urls_to_skip');

    $skip_urls[] = '/admin';
    $skip_urls[] = '/admin/*';
    $skip_urls[] = '/age-verification';
    $skip_urls[] = '/user/login';

    // Append the urls to skips with some hardcoded urls.
    $skipPaths = $skip_urls_config . "\r\n" . implode("\r\n", $skip_urls);

    $request_path = $this->currentPath->getPath();

    // Check if paths don't match then redirect to age verification form.
    $match = $this->pathMatcher->matchPath($request_path, $skipPaths);
    $is_front = $this->pathMatcher->isFrontPage();

    // If not front page then append the path alias as a destination parameter.
    if ($is_front == FALSE) {
      $current_uri = $this->request->getCurrentRequest()->getRequestUri();
      $destination = '?destination=' . $current_uri;
    }
    else {
      $destination = '';
    }
    // If the requested path is not restricted.
    if ($match == TRUE) {
      return;
    }
    // Redirect to the /age-verification with the destination.
    elseif ($match == FALSE) {
      $redirect = new RedirectResponse('/age-verification' . $destination);
      $event->setResponse($redirect);
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // eg: I am using KernelEvents constants (see below a full list).
    $events[KernelEvents::RESPONSE][] = ['onRespond'];
    return $events;
  }

}
