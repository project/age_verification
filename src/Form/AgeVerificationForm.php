<?php

namespace Drupal\age_verification\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Http\RequestStack;
use Drupal\Core\Form\FormBase;

/**
 * Class of Age Verification Form.
 */
class AgeVerificationForm extends FormBase {

  /**
   * The request method.
   *
   * @var \Drupal\Core\Http\RequestStack
   */
  protected $request;

  /**
   * Constructs a new request property.
   *
   * @param \Drupal\Core\Http\RequestStack $request
   *   The request property.
   */
  public function __construct(RequestStack $request) {
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('request_stack'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'age_verification_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('age_verification.settings');

    $form['dob'] = [
      '#title' => $this->t('Please enter your date of birth'),
      '#type' => 'date',
      '#default_value' => !empty($form_state->getValue(['dob'])) ? $form_state->getValue(['dob']) : [],
      '#required' => TRUE,
    ];
    $form['confirmation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('I confirm that this is my age'),
      '#required' => TRUE,
    ];
    // Output the admin description text in the form if it was set.
    if (!empty($config->get('age_verification_description'))) {
      $form['custom_age_verification_description'] = [
        '#type' => 'markup',
        '#markup' => '<p>' . $config->get('age_verification_description') . '</p>',
      ];
    }
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('age_verification.settings');
    $defaultAge = $config->get('age_verification_age_limit');
    $form_values = $form_state->getValues();

    $dob = [];
    [$dob['year'], $dob['month'], $dob['day']] = explode('-', $form_values['dob']);

    if (!empty($dob['month'])) {
      // Get rid of future dates.
      if ((int) $dob['year'] > date('Y')) {
        $dob['year'] = date('Y');
      }
      // We are going to run off midnight for these calculations.
      // Set $date_now to the unix time of today at midnight. This depends on
      // your server settings.
      $date_now = strtotime('today midnight');
      // Form values of day month year are converted to unix time.
      $date_posted = strtotime($form_values['dob']);
      // Simple math calculationt to determine difference.
      $difference = $date_now - $date_posted;
      // Add the Age to $accepted_age with a default of 21.
      $accepted_age = (int) $defaultAge * 31556926;
      // Compare the accepted_age with years of difference.
      if ($difference <= $accepted_age) {
        // Throw an error if user age is less than the age selected.
        // !variable: Inserted as is, with no sanitization or formatting.
        $form_state->setErrorByName('dob', $this->t('You need to be @age or over to access the site.', [
          '@age' => (int) $defaultAge,
        ]));
      }
    }

    // Throws error if user hasn't confirmed their age.
    if (!empty($form_values['confirmation']) && $form_values['confirmation'] != 1) {
      $form_state->setErrorByName('confirmation', $this->t('You need to confirm your age.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Add TRUE to session age_verified.
    $session = $this->request->getCurrentRequest()->getSession();
    $session->set('age_verified', 1);
    // Add a redirect to requested page. Using $form_state built in redirects.
    $redirect = $session->get('age_verification_path');
    if (!empty($redirect)) {
      $form_state->setRedirect($redirect);
    }

    // For everything else, redirect to homepage.
    $form_state->setRedirect('<front>');
  }

}
